# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=typst
pkgver=0.7.0
pkgrel=0
pkgdesc="New markup-based typesetting system that is powerful and easy to learn"
url="https://github.com/typst/typst"
# s390x: 90% of tests fail due to endianness
# armhf: fails build
# s390x, ppc64le, riscv64: ring
arch="all !s390x !ppc64le !riscv64 !armhf"
license="Apache-2.0"
makedepends="
	cargo
	cargo-auditable
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/typst/typst/archive/refs/tags/v$pkgver.tar.gz"
options="net"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	# XXX: it vendors 5MB of fonts, but the font detection is kinda
	# broken and it won't find math symbols from system fonts correctly
	# (when you have a lot of fonts it seems?)
	# so, just keep them vendored-in for now. when fixed, add
	# --no-default-features
	GEN_ARTIFACTS="./gen" \
	TYPST_VERSION=$pkgver \
		cargo auditable build --release --frozen -p typst-cli
}

check() {
	cargo test --frozen --all
}

package() {
	install -Dm755 target/release/typst \
		-t "$pkgdir"/usr/bin/

	install -Dm644 ./crates/typst-cli/gen/typst.bash "$pkgdir"/usr/share/bash-completion/completions/typst
	install -Dm644 ./crates/typst-cli/gen/typst.fish "$pkgdir"/usr/share/fish/vendor_completions.d/typst.fish
	install -Dm644 ./crates/typst-cli/gen/_typst "$pkgdir"/usr/share/zsh/site-functions/_typst
	install -Dm644 ./crates/typst-cli/gen/*.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
49e637ce4b7963808f1b520df331404c6b86a144125158643533290515d11bf6c991a9b7cfaa909104d451b80e0ac4eeac15581e831163942e63c61848ca5f18  typst-0.7.0.tar.gz
"
