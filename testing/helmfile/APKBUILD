# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=helmfile
pkgver=0.156.0
pkgrel=0
pkgdesc="Declarative spec for deploying helm charts"
url="https://helmfile.readthedocs.io/"
arch="all"
license="MIT"
depends="helm"
makedepends="go"
checkdepends="bash"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/helmfile/helmfile/archive/refs/tags/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local _goldflags="
	-X go.szostok.io/version.version=$pkgver
	-X go.szostok.io/version.buildDate=$(date -u "+%Y-%m-%dT%H:%M:%S%z" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	-X go.szostok.io/version.commitDate=$(date -u "+%Y-%m-%dT%H:%M:%S%z" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	-X go.szostok.io/version.commit=0000000
	-X go.szostok.io/version.dirtyBuild=false
	-X github.com/helmfile/helmfile/pkg/runtime.v1Mode=true
	"
	go build -v -o helmfile -ldflags "$_goldflags"

	for shell in bash fish zsh; do
		./helmfile completion $shell > $pkgname.$shell
	done
}

check() {
	# e2e/template test requires docker
	go test $(go list ./... | grep -v /e2e)
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname

	install -Dm644 helmfile.bash \
		"$pkgdir"/usr/share/bash-completion/completions/helmfile
	install -Dm644 helmfile.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_helmfile
	install -Dm644 helmfile.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/helmfile.fish
}

sha512sums="
cea2809a0a74390a044fd438f3ee7152d05b9571dc6ee1bb0f18511a29b88e7bc1ad382434f268bfaea164aa265fb4f796b5bbdd2cfbc092c6ef4e0408eb4b41  helmfile-0.156.0.tar.gz
"
