# Contributor: Coco Liliace <coco@liliace.dev>
# Maintainer: Coco Liliace <coco@liliace.dev>
pkgname=xremap-sway
pkgver=0.8.7
pkgrel=0
pkgdesc="Key remapper for X11 and Wayland"
url="https://github.com/k0kubun/xremap"
# nix
arch="all !s390x !riscv64"
license="MIT"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/k0kubun/xremap/archive/v$pkgver.tar.gz"
builddir="$srcdir/xremap-$pkgver"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --features sway
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin ./target/release/xremap
}

sha512sums="
0c3496ff8134584a6baec3902cfa6d8c80936d61ed331b1810108875dc185b30352ea8cc386988a9c14c633304309ca2cd6d68482358fb47b249b12d1b867ed1  xremap-sway-0.8.7.tar.gz
"
