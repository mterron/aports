# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=rocs
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/education/org.kde.rocs"
pkgdesc="Graph Theory IDE"
license="GPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	boost-dev
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kitemviews-dev
	ktexteditor-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/education/rocs.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/rocs-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# TestTgfFileFormat, TestRocs1FileFormat, TestRocs2FileFormat, and
	# graphtheory-test_graphoperations are broken
	# TestProject requires OpenGL
	local skipped_tests="("
	local tests="
		TestTgfFileFormat
		TestRocs1FileFormat
		TestRocs2FileFormat
		graphtheory-test_graphoperations
		TestProject"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	xvfb-run ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0e386a206289374b7a9e012c2d83df5342262c18d73a7b2a8e5129425bb24fb540e2a6a306fb409278084df3a112f5a3a0abb75e316915bfaac7f0413718d11e  rocs-23.08.0.tar.xz
"
