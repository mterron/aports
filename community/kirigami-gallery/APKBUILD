# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kirigami-gallery
pkgver=23.08.0
pkgrel=0
arch="all !armhf" # armhf blocked by kirigami2 -> qt5-qtdeclarative
url="https://kde.org/applications/development/org.kde.kirigami2.gallery"
pkgdesc="Gallery application built using Kirigami"
license="LGPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kirigami2-dev
	kitemmodels-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/kirigami-gallery.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kirigami-gallery-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3fd4913ee82ae95b24e439d007c52321091bb42b492b4d841ff342dbd67247d81a615d001c7f08c938441e9b70bbd290055874eb690a659674fc6b0944700642  kirigami-gallery-23.08.0.tar.xz
"
