# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-kubernetes
_pkgname=kubernetes
pkgver=27.2.0
pkgrel=0
pkgdesc="Official Python client library for kubernetes"
url="https://github.com/kubernetes-client/python"
arch="noarch"
license="Apache-2.0"
depends="
	py3-certifi
	py3-dateutil
	py3-google-auth
	py3-requests
	py3-requests-oauthlib
	py3-six
	py3-urllib3
	py3-websocket-client
	py3-yaml
	"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="pytest py3-mock"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/k/kubernetes/kubernetes-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# deselected tests have missing modules
	.testenv/bin/python3 -m \
	pytest --ignore=kubernetes/dynamic/test_client.py \
		--ignore=kubernetes/dynamic/test_discovery.py \
		--ignore=kubernetes/e2e_test \
		kubernetes
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
afd25d623d1916a56af5eea36bf83bf121160d3d2326307c50f3bb8cbb08085ada0a5f21e328b384cdcb153d30585b49401ec8c3e1d57ad9703e18c8d8a85375  py3-kubernetes-27.2.0.tar.gz
"
