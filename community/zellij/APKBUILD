# Contributor: guddaff <guddaff@protonmail.com>
# Maintainer: guddaff <guddaff@protonmail.com>
pkgname=zellij
pkgver=0.38.1
pkgrel=0
pkgdesc="Terminal multiplexer"
url="https://zellij.dev/"
license="MIT"
# armv7, armhf: warning: inline asm clobber list contains reserved registers: D16, D17, D18, D19, D20, D21, D22, D23, D24, D25, D26, D27, D28, D29, D30, D31
# x86: error[E0425]: cannot find value `REG_EIP` in crate `libc`
# s390x, ppc64le : corosensei-0.1.3 unsupported target
arch="all !armhf !armv7 !ppc64le !s390x !x86"
makedepends="
	cargo
	cargo-auditable
	mandown
	openssl-dev>3
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/zellij-org/zellij/archive/v$pkgver/zellij-$pkgver.tar.gz"
options="!check" # troublesome

prepare() {
	default_prepare

	# Fix 'undefined reference to "open64"'
	cargo update -p getrandom@0.2.7 --precise 0.2.10

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release

	mandown docs/MANPAGE.md > target/$pkgname.1

	mkdir -p target/completions
	for sh in bash fish zsh; do
		target/release/$pkgname setup --generate-completion $sh \
			> target/completions/$pkgname.$sh
	done
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/

	install -D -m644 target/$pkgname.1 -t "$pkgdir"/usr/share/man/man1/

	install -D -m644 target/completions/$pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname

	install -D -m644 target/completions/$pkgname.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish

	install -D -m644 target/completions/$pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
08a7151d26249ef746022a04f2163942ec5acdc7840623a450ee2e2ab32297afc5b2fcc52450438211f8d39785c6bfbf2c6dcd84ea1753c5771f1d9e62f99847  zellij-0.38.1.tar.gz
"
