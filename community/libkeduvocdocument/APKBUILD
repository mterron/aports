# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkeduvocdocument
pkgver=23.08.0
pkgrel=0
arch="all !armhf"
url="https://edu.kde.org"
pkgdesc="Library to parse, convert, and manipulate KVTML files"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/education/libkeduvocdocument.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkeduvocdocument-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6c4b3ec748ebcaf7c29338d28213de45ae25d0ce24e169f39d836fd7e37eaf52e142fafdd689034a0c57d231a20669d5b12dcde8612079cb47679c85d439171a  libkeduvocdocument-23.08.0.tar.xz
"
