# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=katomic
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/org.kde.katomic"
pkgdesc="A fun and educational game built around molecular geometry"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	kcoreaddons-dev
	kconfig-dev
	kcrash-dev
	kwidgetsaddons-dev
	ki18n-dev
	kxmlgui-dev
	knewstuff-dev
	kdoctools-dev
	kdbusaddons-dev
	libkdegames-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/katomic.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/katomic-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
664e720c730f78862215dd291a4dec78a263f3ca718138a622edbe0089ed7c021a72881dd39dcbf8c582e783f9cde3ade108151db462e5337a55c2cc54c13286  katomic-23.08.0.tar.xz
"
